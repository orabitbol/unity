using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBars : MonoBehaviour
{
    public Transform target;
 
    public void Update()
        {
            var wantedPos = Camera.main.WorldToViewportPoint(target.position);
            transform.position = wantedPos;
        }
}
