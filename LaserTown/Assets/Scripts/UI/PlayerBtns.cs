using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBtns : GenericSingletonClass_canvas<MonoBehaviour>
{
    //All the actions a player can do through pressing buttons
    public FireGear fireGear; //TODO: this class will only refarance the game manager and will activate other scripts through it
    public Button fireBtn;
    public List<Button> modBtnsList;
    public GenericGameEntity player;
    public GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
    }
    public void FireProjectile()
    {
        fireGear.FireProjectile();
        fireBtn.interactable = false;
        player.isMyTurn = false;
        DisableModsBtns();
    }

    public void SniperMod()
    {
        gameManager.modsManager.ChangeMod(GunMod.sniper);
        DisableModsBtns();
    }
    public void ShotGunMod()
    {
        gameManager.modsManager.ChangeMod(GunMod.shotgun);
        DisableModsBtns();
    }

    public void MachineGunMod()
    {
        gameManager.modsManager.ChangeMod(GunMod.machinegun);
        DisableModsBtns();
    }

    private void EnableFireBtn() //in your turn
    {
        fireBtn.interactable = true;
    }

    private void DisableModsBtns()
    {
        for(int i = 0; i< modBtnsList.Count; i++)
        {
            modBtnsList[i].interactable = false;
        }
    }

    private void EnableModsBtns()
    {
        for (int i = 0; i < modBtnsList.Count; i++)
        {
            modBtnsList[i].interactable = true;
        }
    }

    public void PlayerSetUp()
    {
        EnableModsBtns();
        EnableFireBtn();
    }

    public void StartGame()
    {
        gameManager.levelManager.LoadLevel(1);
    }

    public void Quit()
    {
        Application.Quit();
    }

}
