using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeBee : GenericEnemy
{
    public override void SpecificAttack()
    {
        showManager.GetComponent<ShowManager>().ShowEnemyProjectile(this.transform);
    }
}
