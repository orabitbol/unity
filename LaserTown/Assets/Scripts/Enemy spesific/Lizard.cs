using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lizard : GenericEnemy
{
    public override void SpecificAttack()
    {
        //do animation
        //do dmg to player
        playerPos.GetComponent<GenericHp>().TakeDmg(attackDmg);
    }
}
