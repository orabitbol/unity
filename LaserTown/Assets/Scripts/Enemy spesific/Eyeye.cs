using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eyeye : GenericEnemy
{
    public override void SpecificAttack()
    {
        showManager.GetComponent<ShowManager>().ShowEnemyProjectile(this.transform);
    }
}
