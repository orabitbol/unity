using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameProjectile : MonoBehaviour
{
    public Rigidbody2D myRb;
    public float projectileSpeed;
    private bool goingUp;
    private float angleBounceAdjustment; //with the current laser sprite this will be 60 or -60
    public static Action projectileReached;
    public static Action batteryHit;
   [SerializeField] private int bounceCounterThreshHold = 30; //limit on bounces so the projectile wont get stuck
    private int bounceCounter;

    private void Start()
    {
        if (!myRb)
        {
            Debug.LogError("Laser Projectile " + transform.name + " Rigidbody2d component not found");
        }
        bounceCounter = bounceCounterThreshHold;
    }


    public void Fire(Vector2 velocity, bool isGoingUp, float zRotation) //move in a given direction
    {
        bounceCounter = bounceCounterThreshHold;
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, zRotation);
        myRb.AddForce(velocity * projectileSpeed);
        goingUp = isGoingUp;
        if (goingUp)
        {
            angleBounceAdjustment = 0;
        }
        else
        {
            angleBounceAdjustment = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if (other.transform.CompareTag("gunBound")) //change facing direction
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.eulerAngles.z + angleBounceAdjustment));
        }
        angleBounceAdjustment *= -1;
        bounceCounter--;
        if(bounceCounter <= 0)
        {
            //Debug.Log("Helping to unstuck");
            bounceCounter = bounceCounterThreshHold;
            myRb.AddForce(Vector2.right * projectileSpeed);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) //begin visual projectile 
    {
        if (other.transform.CompareTag("gunEnd"))
        {
            projectileReached?.Invoke();
            Destroy(this.gameObject);
        }
        else if (other.transform.CompareTag("battery") && !other.gameObject.GetComponent<GenericBattery>().batteryActive)
        {
            //Send a signal to the dmg counter
            batteryHit?.Invoke();

            //Change the sprite and bool on the battery
            other.gameObject.GetComponent<GenericBattery>().batteryActive = true;
            other.gameObject.GetComponent<GenericBattery>().ChangeSprite();
        }
    }
}
