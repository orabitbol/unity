using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : VisualProjectile
{
    public override void Fire(List<GameObject> targets) 
    {
        targets[0].GetComponent<PlayerHp>().TakeDmg(projectileDmg);
    }

}
