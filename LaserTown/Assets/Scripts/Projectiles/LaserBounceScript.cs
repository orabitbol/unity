using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBounceScript : MonoBehaviour
{
    private Rigidbody2D myRb;
    Vector3 lastVelocity;
    void Start()
    {
        myRb = GetComponent<Rigidbody2D>();
        if (!myRb)
        {
            Debug.LogError("no rigid body found at bounc script");
        }
    }

    void Update()
    {
        lastVelocity = myRb.velocity;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("gunBound"))
        {
            var speed = lastVelocity.magnitude;
            var dir = Vector3.Reflect(lastVelocity.normalized, other.contacts[0].normal);
            myRb.velocity = dir * Mathf.Max(speed, 1f);
            transform.rotation = Quaternion.LookRotation(myRb.velocity, Vector2.up);
            float zRotation;
            if (WrapAngle(transform.rotation.eulerAngles.x) > 0) //if x is positive => (y - x -45) * -1
            {
                zRotation = (WrapAngle(transform.rotation.eulerAngles.y) - WrapAngle(transform.rotation.eulerAngles.x) - 40f) * -1;
            }
            else  //if x negitive => x + y -45 
            {
                zRotation = WrapAngle(transform.rotation.eulerAngles.x) + WrapAngle(transform.rotation.eulerAngles.y) - 40f;
            }
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, zRotation));
        }
    }

    private float WrapAngle(float angle)
    {
        angle %= 360;
        if (angle > 180)
            return angle - 360;

        return angle;
    }

}
