using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VisualProjectile : MonoBehaviour
{
    public Rigidbody2D myRb; //native
    public float projectileSpeed; //native
    public string targetId; //get passed from ShowManager
    public float projectileDmg; //get passed from ShowManager
    public static Action resetDmgCounter;
    public bool moving;
    public GameObject targetObj;

    /*private void Update()
    {
        if (moving)
        {
            transform.position = transform.position * projectileSpeed * Time.deltaTime;
            if (Vector3.Distance(transform.position, targetObj.transform.position) < 0.002f)
            {
                moving = false;
            }
        }
    }*/

    public virtual void Fire(List<GameObject> targets) //move in a given direction
    {
        int numberOfProjectile = targets.Count;
        int dmgPerEnemy = (int)projectileDmg / numberOfProjectile;
        for (int i = 0; i<numberOfProjectile; i++)
        {
            targets[i].GetComponent<EnemyHp>().TakeDmg(dmgPerEnemy);
        }
        resetDmgCounter?.Invoke();
        GenericGameEntity.isMyTurnDone?.Invoke();
        Destroy(this.gameObject);
    }
}
