using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : GenericSingletonClass_Level<MonoBehaviour>
{
    public EnemySetup[] levelsArray;
    public string[] sceneNames;
    public int currentLevel;
    public GameObject enemiesTab;
    public GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
    }

    public void LoadLevel(int level)
    {
        {
            EnemySetup currLvl = levelsArray[level];
            SceneManager.LoadScene(sceneNames[level]);
            currentLevel = level;
            if (level > 0)
            {
                gameManager.turnManager.clearQue();
                for (int i = 0; i < currLvl.enemies.Length; i++)
                {
                    GameObject enemy = Instantiate(currLvl.enemies[i], enemiesTab.transform);
                    enemy.transform.position = new Vector3(currLvl.enemiesPos[i].x, currLvl.enemiesPos[i].y, transform.position.z);
                }
                gameManager.turnManager.BuildQue();
            }
            level++;
            currentLevel = level;
        }

    }

    public void ClearLvl()
    {
        if (currentLevel == 4)
        {
            LoadWin();
        }
        else { LoadLevel(currentLevel); }
    }

    public void LoadWin()
    {
        Application.Quit();
        //no time to make win or lose screen work :(
    }

}
