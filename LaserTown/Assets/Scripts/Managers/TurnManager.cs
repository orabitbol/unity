using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TurnManager : GenericSingletonClass_Turn<MonoBehaviour>
{
    public Queue turnOrderQue = new Queue();
    public GameObject currentTurn;
    public static Action noMoreEnemies;
    public GameManager gameManager;
    private void OnEnable()
    {
        gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
        GenericGameEntity.isMyTurnDone += AssignTurnToFirst;
    }

    private void OnDisable()
    {
        GenericGameEntity.isMyTurnDone -= AssignTurnToFirst;
    }

    public void clearQue() //called when lvl clear
    {
        turnOrderQue.Clear();
    }

    public void BuildQue() //called when lvl load
    {
        gameManager.ActiveGamePanels();
        AddToQue(GameObject.FindWithTag("player"), turnOrderQue);
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        for (int i = 0; i< enemies.Length; i++)
        {
            AddToQue(enemies[i], turnOrderQue);
            Debug.Log(enemies[i]);
        }
        currentTurn = (GameObject)turnOrderQue.Dequeue();
        currentTurn.GetComponent<GenericGameEntity>().isMyTurn = true; //take the first out of the que and give it a turn
        currentTurn.GetComponent<GenericGameEntity>().TurnSetUp();
    }

    public void AddToQue(GameObject addThis, Queue que)
    {
        que.Enqueue(addThis);
    }

    public void AssignTurnToFirst()
    {
        //deque if need
        DequeueIfNeed();
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].GetComponent<GenericEnemy>().targeted = false;
        }
        GenericEnemy.activeTarget?.Invoke();
    }

    public void DequeueIfNeed()
    {
        Queue newQue = new Queue();
        while (turnOrderQue.Count > 0)
        {
            GameObject temp = (GameObject)turnOrderQue.Dequeue();
            if (temp)
            {
                if (temp.GetComponent<GenericHp>().currHp > 0) //filter dead enemies
                {
                    AddToQue(temp, newQue);
                }
            }
        }
        AddToQue(currentTurn, newQue);

        //copy the new que to the the active que
        clearQue();
        do
        {
            GameObject temp = (GameObject)newQue.Dequeue();
            turnOrderQue.Enqueue(temp);
        } while (newQue.Count > 0);

        //end current turn
        currentTurn.GetComponent<GenericGameEntity>().isMyTurn = false;
        //assign new current
        currentTurn = (GameObject)turnOrderQue.Dequeue();
        if(turnOrderQue.Count <= 0 && currentTurn.CompareTag("player"))
        {
            //lvl clear
            clearQue();
            gameManager.levelManager.ClearLvl();

        }
        currentTurn.GetComponent<GenericGameEntity>().isMyTurn = true; //take the first out of the que and give it a turn
        currentTurn.GetComponent<GenericGameEntity>().TurnSetUp();
        
    }
}
