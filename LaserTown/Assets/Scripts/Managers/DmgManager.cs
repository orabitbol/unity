using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class DmgManager : GenericSingletonClass_Dmg<MonoBehaviour>
{
    [SerializeField] private  TextMeshProUGUI dmgText;
    [SerializeField] private int batteryDmgBooster;
    public GameManager gameManager;
    private Action dmgAdded;
    private int currentDmg;


    #region System Functions

    private void OnEnable()
    {
        gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
        dmgAdded += UpdateDmgText;
        GameProjectile.batteryHit += AddToDmgCount;
        VisualProjectile.resetDmgCounter += DmgReset;
    }

    private void OnDisable()
    {
        dmgAdded -= UpdateDmgText;
        GameProjectile.batteryHit -= AddToDmgCount;
        VisualProjectile.resetDmgCounter -= DmgReset;
    }

    void Start()
    {
        currentDmg = 20;
        UpdateDmgText();
    }

    #endregion

    private void UpdateDmgText()
    {
        dmgText.text = "" + currentDmg;
    }

    public void AddToDmgCount()
    {
        currentDmg += batteryDmgBooster;
        gameManager.showManager.dmgAccumulated = currentDmg;
        dmgAdded?.Invoke();
    }

    public void DmgReset()
    {
        currentDmg = 20;
        gameManager.showManager.dmgAccumulated = currentDmg;
        UpdateDmgText();
    }
}
