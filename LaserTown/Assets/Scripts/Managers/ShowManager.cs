using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowManager : GenericSingletonClass_Show<MonoBehaviour>
{
    public string targetId; //in the enemy que
    [SerializeField] private GameObject visualProjectilePrefab; //the actual prefab gameobject of a projectile in the show screen
    [SerializeField] private GameObject enemyProjectilePrefab;
    [SerializeField] private Transform gunPoint; //where to instantiate the projectile
    public float dmgAccumulated; //keeps track of the dmg a projectile need to do and pass it to the new gameObject instatiated
    private void OnEnable()
    {
        //subscribe to the "projectile reachd the end of the barrle" action
        GameProjectile.projectileReached += ShowFireProjectile;
    }

    private void OnDisable()
    {
        //unsubscribe
        GameProjectile.projectileReached -= ShowFireProjectile;
    }

    //debug
    private void Start()
    {
        targetId = "enemy"; // from gun mod
    }
    //debug end

    private void Update()
    {
        //when the action event "target changed" trigger
        //do switch on the "current gun mode" and deside the target
    }

    private void ShowFireProjectile() //gets called when a projectile in the game panel reached the end
    {
        //play animation in the show windows

        //instantiate a Visualprojectile in show window
        GameObject visualProjectileTemp = Instantiate(visualProjectilePrefab, gunPoint.position, Quaternion.identity) as GameObject;
        //pass the currect variables to the spesific projectile
        visualProjectileTemp.GetComponent<VisualProjectile>().targetId = targetId;
        visualProjectileTemp.GetComponent<VisualProjectile>().projectileDmg = dmgAccumulated;
        //collect targets
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        List<GameObject> targets = new List<GameObject>();
        for(int i = 0; i< enemies.Length; i++)
        {
            if (enemies[i].GetComponent<GenericEnemy>().targeted)
            {
                targets.Add(enemies[i]);
            }
        }

        //fire
        visualProjectileTemp.GetComponent<VisualProjectile>().Fire(targets);
    }

    public void ShowEnemyProjectile(Transform enemyTrans)
    {
        //instantiate a Visualprojectile in show window
        GameObject visualProjectileTemp = Instantiate(enemyProjectilePrefab, enemyTrans.position, Quaternion.identity) as GameObject;
        //pass the currect variables to the spesific projectile
        visualProjectileTemp.GetComponent<VisualProjectile>().projectileDmg = enemyTrans.GetComponent<GenericEnemy>().attackDmg;
        //collect targets
        GameObject player = GameObject.FindGameObjectWithTag("player");
        List<GameObject> targets = new List<GameObject>();
        targets.Add(player);
        //fire
        visualProjectileTemp.GetComponent<EnemyProjectile>().Fire(targets);
    }
}
