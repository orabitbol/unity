using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : GenericSingletonClass_GameManager<MonoBehaviour>
{
    public TurnManager turnManager;
    public DmgManager dmgManager;
    public ModsManager modsManager;
    public LevelManager levelManager;
    public ShowManager showManager;
    public PlayerBtns uiManager;
    public GameObject viewFrame;
    public GameObject gunFrame;
    public GameObject menuCanvas;
    public GameObject GameFrame;

    public void StartGame()
    {
        levelManager.currentLevel = 1;
        levelManager.LoadLevel(levelManager.currentLevel);
    }

    public void ActiveGamePanels()
    {
        viewFrame.SetActive(true);
        gunFrame.SetActive(true);
        menuCanvas.SetActive(false);
        GameFrame.SetActive(true);
    }

    public void DeactivePanels()
    {
        viewFrame.SetActive(false);
        gunFrame.SetActive(false);
        menuCanvas.SetActive(true);
        GameFrame.SetActive(false);
    }
    
}
