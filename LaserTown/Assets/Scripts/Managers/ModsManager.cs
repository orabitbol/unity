using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GunMod
{
    sniper,
    shotgun,
    machinegun
}

public class ModsManager : GenericSingletonClass_Mods<MonoBehaviour>
{
    [SerializeField] private GunMod currentGunMod;
    [SerializeField] private GameObject batteryPrefab;
    [SerializeField] private List<GameObject> currentBatteriesList;
    [SerializeField] private RandomSpawnFrame currentSpawnFrame;

    //mods
    [SerializeField] private RandomSpawnFrame sniperFrameTop;
    [SerializeField] private RandomSpawnFrame sniperFrameBottom;
    [SerializeField] private RandomSpawnFrame shotGunFrameFront;
    [SerializeField] private RandomSpawnFrame machineGunFrameFront;
    [SerializeField] private RandomSpawnFrame machineGunFrameBack;

    //mods sprites
    [SerializeField] private GameObject modSprite;
    [SerializeField] private Sprite sniperMod;
    [SerializeField] private Sprite shotGunMod;
    [SerializeField] private Sprite machineGunMod;

    private Sprite currentModSprite;
    public Transform playerTrans;

    private void OnEnable()
    {
        VisualProjectile.resetDmgCounter += ClearModSelection;
    }

    private void OnDisable()
    {
        VisualProjectile.resetDmgCounter -= ClearModSelection;
    }

    public void ChangeMod(GunMod newMod)
    {
        if(currentGunMod != newMod)
        {
            currentGunMod = newMod;
        }
        UpdateBatteries();
    }

    private void ClearModSelection()
    {
        ClearBatteryList();
        modSprite.SetActive(false);
    }

    private void UpdateBatteries() //point 1 (index 0) holds min x and max y point 3 (index 2)holds max x min y
    {
        RandomSpawnFrame currentSpawnFrame2 = null;
        ClearBatteryList();
        switch (currentGunMod)
        {
            case GunMod.sniper:
                int topOrDown = Random.Range(0, 2);
                if (topOrDown == 1)
                {
                    currentSpawnFrame = sniperFrameTop;
                }
                else
                {
                    currentSpawnFrame = sniperFrameBottom;
                }
                currentModSprite = sniperMod;
                SetTargets(GunMod.sniper);
                break;
            case GunMod.shotgun:
                currentSpawnFrame = shotGunFrameFront;
                currentModSprite = shotGunMod;
                SetTargets(GunMod.shotgun);
                break;
            case GunMod.machinegun:
                currentSpawnFrame = machineGunFrameFront;
                currentModSprite = machineGunMod;
                currentSpawnFrame2 = machineGunFrameBack;
                SetTargets(GunMod.machinegun);
                break;
            default:
                currentSpawnFrame = null;
                Debug.LogError("no spawn frame match the current gun mod!");
                break;
        }
        //set the frame Min and Max based on the current gun mod
        float maxX = currentSpawnFrame.areasOfSpawn[2].x;
        float maxY = currentSpawnFrame.areasOfSpawn[0].y;
        float minX = currentSpawnFrame.areasOfSpawn[0].x;
        float minY = currentSpawnFrame.areasOfSpawn[2].y;
       
        for (int i = 0; i< currentSpawnFrame.numberOfBatteries; i++)
        {
            //roll a numbers in that range
            float xPos = Random.Range(minX, maxX);
            float yPos = Random.Range(minY, maxY);
            //check if there is already a battery nearby
            int iterations = 0;
            bool isOccupied = CheckIfOccupied(xPos, yPos);
            while (isOccupied)
            {
                //roll new numbers
                xPos = Random.Range(minX, maxX);
                yPos = Random.Range(minY, maxY);
                if (iterations > 100) 
                {
                    Debug.LogError("Max iteration reachd, cannot find a valid location to the battery");
                    break;
                }
                iterations++;
                isOccupied = CheckIfOccupied(xPos, yPos);
            }
            if (!isOccupied)
            {
                //add to the list 
                currentBatteriesList.Add(Instantiate(batteryPrefab, new Vector3(xPos, yPos, 1), Quaternion.identity));
            }
           
        }
        modSprite.SetActive(true);
        modSprite.GetComponent<SpriteRenderer>().sprite = currentModSprite;

        if (currentSpawnFrame2)
        {
            //set the frame Min and Max based on the current gun mod
            float maxX2 = currentSpawnFrame2.areasOfSpawn[2].x;
            float maxY2 = currentSpawnFrame2.areasOfSpawn[0].y;
            float minX2 = currentSpawnFrame2.areasOfSpawn[0].x;
            float minY2 = currentSpawnFrame2.areasOfSpawn[2].y;

            for (int i = 0; i < currentSpawnFrame.numberOfBatteries; i++)
            {
                //roll a numbers in that range
                float xPos = Random.Range(minX2, maxX2);
                float yPos = Random.Range(minY2, maxY2);
                //check if there is already a battery nearby
                int iterations = 0;
                bool isOccupied = CheckIfOccupied(xPos, yPos);
                while (isOccupied)
                {
                    //roll new numbers
                    xPos = Random.Range(minX2, maxX2);
                    yPos = Random.Range(minY2, maxY2);
                    if (iterations > 100)
                    {
                        Debug.LogError("Max iteration reachd, cannot find a valid location to the battery");
                        break;
                    }
                    iterations++;
                    isOccupied = CheckIfOccupied(xPos, yPos);
                }
                if (!isOccupied)
                {
                    //add to the list 
                    currentBatteriesList.Add(Instantiate(batteryPrefab, new Vector3(xPos, yPos, 1), Quaternion.identity));
                }
            }
        }
    }

    private void ClearBatteryList()
    {
        for(int i = 0; i< currentBatteriesList.Count; i++)
        {
            Destroy(currentBatteriesList[i]);
        }
        currentBatteriesList.Clear();
    }

    private bool CheckIfOccupied(float xPos, float yPos) 
    {
        for (int i = 0; i< currentBatteriesList.Count; i++)
        {
            float x = currentBatteriesList[i].transform.position.x;
            float y = currentBatteriesList[i].transform.position.y;
            if(Mathf.Abs(x - xPos) <= currentSpawnFrame.batterySpreadX && Mathf.Abs(y - yPos) <= currentSpawnFrame.batterySpreadY)
            {
                return true;
            }
        }
        return false;
    }

    #region Targets Settings
    private void SetTargets(GunMod mode)
    {
       
        switch (mode) {
            case GunMod.shotgun:
                //target 1 closest flyer and ground enemy
                GameObject enemy1 = GetClosestOfType(TypeOfEnemy.flayer);
                if (enemy1)
                {
                    enemy1.GetComponent<GenericEnemy>().targeted = true;
                    GenericEnemy.activeTarget?.Invoke();
                }
                GameObject enemy2 = GetClosestOfType(TypeOfEnemy.land);
                if (enemy2)
                {
                    enemy2.GetComponent<GenericEnemy>().targeted = true;
                    GenericEnemy.activeTarget?.Invoke();
                }

                break;
            case GunMod.sniper:
                //target closest enemy
                GameObject cEnemy = GetClosest();
                if (cEnemy)
                {
                    cEnemy.GetComponent<GenericEnemy>().targeted = true;
                    GenericEnemy.activeTarget?.Invoke();
                }
                break;
            case GunMod.machinegun:
                //target 1 Randoms
                GameObject rEnemy = GetRandom();
                rEnemy.GetComponent<GenericEnemy>().targeted = true;
                GenericEnemy.activeTarget?.Invoke();
                break;
        }
    }

    private GameObject GetClosestOfType(TypeOfEnemy type)
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        GameObject result = null;
        float minDist = Mathf.Infinity;
        for (int i = 0; i < enemies.Length; i++)
        {
            float dist = Mathf.Abs((enemies[i].GetComponent<Transform>().position.x - playerTrans.position.x));
            if (enemies[i].GetComponent<GenericEnemy>().TypeOfEnemy == type && dist < minDist)
            {
                result = enemies[i];
                minDist = dist;
            }
        }
        return result;
    }

    private GameObject[] GetAllOfType(TypeOfEnemy type)
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        GameObject[] result = null;
        int rIndex = 0;
        for (int i = 0; i < enemies.Length; i++)
        {
            if (enemies[i].GetComponent<TypeOfEnemy>().Equals(type))
            {
                result[rIndex] = enemies[i];
                rIndex++;
            }
        }
        return result;
    }

    private GameObject GetClosest()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        GameObject result = null;
        float minDist = Mathf.Infinity;
        for (int i = 0; i < enemies.Length; i++)
        {
            float dist = Mathf.Abs((enemies[i].GetComponent<Transform>().position.x - playerTrans.position.x));
            if (dist < minDist)
            {
                result = enemies[i];
                minDist = dist;
            }
        }
        return result;
    }

    private GameObject GetRandom()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        int rnd = Random.Range(0, enemies.Length - 1);
        return (enemies[rnd]);
    }

    #endregion

}
