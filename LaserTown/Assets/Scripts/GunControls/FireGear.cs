using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGear : MonoBehaviour
{
    [Header("Objects Refarances")]
    [SerializeField] private GameObject gearWheel; //the rotating gear inside the gun
    [SerializeField] private Transform gearTip; //the starting position of wich we instantiate the projectiles
    [SerializeField] private GameObject laserProjectilePrefab;
    [SerializeField] private Rigidbody2D myRb;

    [Header("Gear Variables")] 
    public float gearSpeed; //TODO: will be ajusted by the "GunMod manager"
    public bool gearMoving; //Based on player input or lack of it
    [SerializeField] private float shootingErrorMargin; //how much of a diffrance is allowed to make a stright shot

    [Header("Serialized For Debugging")]
    [SerializeField] private float curentAngle;
    [SerializeField] private Vector3 movingDir; //the current moving direction of the gear
    [SerializeField] private float maxAngle;
    [SerializeField] private float minAngle;
    [SerializeField] private bool isShootingUp;

    #region Class main Functions 
    //start, updates and enables
    private void Start()
    {
        movingDir = Vector3.forward;
    }
    private void Update()
    {
        curentAngle = WrapAngle(gearWheel.transform.eulerAngles.z); //the angle as shown in the inspector (the z rotation of gearWheel)
        if (gearMoving)
        {
            if (curentAngle >= maxAngle) //reached upper bound, change direction
            {
                movingDir = Vector3.back;
            }
            else if (curentAngle <= minAngle) //reached lower bound, change direction
            {
                movingDir = Vector3.forward;
            }
            MoveGear(movingDir);
        }
    }

    #endregion 


    #region General Functions
    //Class spesific functions
    private void MoveGear(Vector3 dir) //moves the gear up or down (Vector3.forward,Vector3.backward)
    {
        gearWheel.transform.Rotate(dir, gearSpeed * Time.deltaTime);
    }

    public void FireProjectile() //instantiate a new laser projectile and give him a push
    {
        //stop the spinning gear
        //gearMoving = false;

        //fire the projectile 
        GameObject currentProjectile = Instantiate(laserProjectilePrefab, gearTip.position, Quaternion.Euler(new Vector3(0, 0, gearWheel.transform.rotation.eulerAngles.z -45f))) as GameObject;
        float yDir;

        //calculate slope angle => arctan( (y2-y1) / (x2-x1) ) + 180 
        Vector2 point1 = new Vector2(transform.position.x, transform.position.y);
        Vector2 point2 = new Vector2(gearTip.position.x, gearTip.position.y);
        yDir =Mathf.Atan((point1.y - point2.y) / (point1.x - point2.x));
        if (curentAngle > -45f) //shooting up
        {
            isShootingUp = true;
        }
        else if (curentAngle < -45f) //shooting down
        {
            isShootingUp = false;
        }
        else if ((curentAngle >= -45 - shootingErrorMargin) && (curentAngle <= -45 + shootingErrorMargin))
        {
            yDir = 0;
        }
        //Debug.Log("ydir is: " + yDir);
        currentProjectile.GetComponent<GameProjectile>().Fire(new Vector2(2, yDir), isShootingUp, curentAngle + 45f);
    }
    #endregion


    #region Helper functions
    //Calculations and private functions used by the general functions (should not be called outside this class)
    private float WrapAngle(float angle)
    {
        angle %= 360;
        if (angle > 180)
            return angle - 360;

        return angle;
    }

    
    #endregion
}
