using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : GenericGameEntity
{
    // add stuff to the player
    public PlayerBtns playerBtns;

    public override void TurnSetUp()
    {
        playerBtns.PlayerSetUp();
    }
}
