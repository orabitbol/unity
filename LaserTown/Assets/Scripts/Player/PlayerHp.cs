using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHp : GenericHp
{
    public GameManager gameManager;
    public override void Die()
    {
        Debug.Log("Game over!");
        Application.Quit();
    }
}
