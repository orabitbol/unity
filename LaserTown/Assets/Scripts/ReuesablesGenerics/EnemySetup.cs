using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "NewEnemySetup")]
public class EnemySetup : ScriptableObject
{
    public GameObject[] enemies;
    public Vector3[] enemiesPos;
}
