using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum TypeOfEnemy
{
    land,
    flayer,
    boss
}

public class GenericEnemy : GenericGameEntity
{
    private bool moving;
    private  Vector3 targetDestination;
    public GameObject markedTarget; //sprite is enabled when the enemy is targeted
    public bool targeted;
    public static Action activeTarget;
    public TypeOfEnemy TypeOfEnemy;
    public float attackRange;
    public Transform playerPos;
    public GameObject showManager;
    public int attackDmg;
    public bool isInRange;
    public bool attacked;

    private void OnEnable()
    {
        activeTarget += isTargeted;
    }

    private void OnDisable()
    {
        activeTarget -= isTargeted;
    }

    private void Start()
    {
        attacked = false;
        isInRange = false;
        targeted = false;
        isTargeted();
        playerPos = GameObject.FindGameObjectWithTag("player").transform;
        showManager = GameObject.FindGameObjectWithTag("showManagaer");
    }
    public virtual void Update()
    {
        if (moving)
        {
            if (!isInRange)
            {
                transform.position = new Vector3(transform.position.x - 0.001f, transform.position.y, transform.position.z);
                if ((Vector3.Distance(transform.position, targetDestination) <= 0.002f) || Mathf.Abs(transform.position.x - playerPos.position.x) <= attackRange) //reached destenation
                {
                    myAnim.SetBool("isMoving", false);
                    moving = false;
                    if (Mathf.Abs(transform.position.x - playerPos.position.x) <= attackRange && !attacked)
                    {
                        //do attack
                        SpecificAttack();
                        isInRange = true;
                        attacked = true;
                    }
                    isMyTurnDone?.Invoke();
                }
            }
            else
            {
                if (!attacked)
                {
                    SpecificAttack();
                    attacked = true;
                    isMyTurnDone?.Invoke();
                }
            }
            
        }

    }
    public override void TurnSetUp()
    {
        targeted = false;
        attacked = false;
        isTargeted();
        //move
        moving = true;
        myAnim.SetBool("isMoving", true);
        targetDestination = new Vector3(transform.position.x - 3, transform.position.y, transform.position.z); //3 magic number
    }

    public virtual void SpecificAttack()
    {
        // in child class
    }

    public void isTargeted()
    {
        markedTarget.SetActive(targeted);
    }
}
