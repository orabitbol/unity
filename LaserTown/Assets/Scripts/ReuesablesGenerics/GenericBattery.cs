using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericBattery : MonoBehaviour
{
    public bool batteryActive;
    public Sprite activeSprite;
    public Sprite unActiveSprite;

    public void Start()
    {
        batteryActive = false;
        ChangeSprite();
    }

    public void ChangeSprite()
    {
        if (batteryActive)
        {
            GetComponent<SpriteRenderer>().sprite = activeSprite;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = unActiveSprite;
        }
    }
}

