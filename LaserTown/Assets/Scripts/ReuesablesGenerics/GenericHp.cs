using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GenericHp : MonoBehaviour
{
    public float maxHp;
    public float currHp;
    public GameObject hpBar;
    public TextMeshPro hpText;

    void Start()
    {
        currHp = maxHp;
        hpBar.transform.localScale = new Vector3(1f, hpBar.transform.localScale.y, hpBar.transform.localScale.z);
        hpText.text = currHp + " / " + maxHp;
    }


    public virtual void Heal(float amount)
    {
        currHp += amount;
        if (currHp > maxHp) //overheal
        {
            currHp = maxHp;
        }
    }

    public virtual void FullHeal()
    {
        currHp = maxHp;
    }

    public virtual void TakeDmg(float amount)
    {
        currHp -= amount;
        if (currHp <= 0) //overdmg
        {
            currHp = 0;
            Die();
        }
        hpBar.transform.localScale = new Vector3((currHp / maxHp), hpBar.transform.localScale.y, hpBar.transform.localScale.z);
        hpText.text = currHp + " / " + maxHp;
    }

    public virtual void Die()
    {
        Destroy(this.gameObject);
    }
}
