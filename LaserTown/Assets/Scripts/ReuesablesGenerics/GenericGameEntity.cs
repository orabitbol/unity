using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GenericGameEntity : MonoBehaviour
{
    //all Playable gameobject will have that
    public bool isMyTurn;
    public static Action isMyTurnDone; //when called remember to insert yourself into the que
    public Animator myAnim;

    public virtual void TurnSetUp()
    {
        //ends with isMyTurnDone?.Invoke();
    }
}
