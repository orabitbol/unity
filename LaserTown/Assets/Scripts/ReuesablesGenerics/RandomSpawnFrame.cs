using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "NewBatteriesFrame")]
public class RandomSpawnFrame : ScriptableObject
{
    public List<Vector3> areasOfSpawn;
    public int numberOfBatteries;
    public float batterySpreadX;
    public float batterySpreadY;
}
